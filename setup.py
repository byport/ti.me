from setuptools import setup
setup(
    name='ti.me',
    version='0.3.6',
    description=(
        'Manage, organize, share and plan your tasks'
        ' using ti.me task manager.'
    ),
    url='https://bitbucket.org/byport/ti.me',
    author='Vladimir Sernatsky',
    author_email='byport1112@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Utilities',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='alpha time management cli',
    packages=['timelib', 'timelib.models', 'timecli'],
    install_requires=['sqlalchemy', 'python-dateutil', 'termcolors'],
    entry_points={
        'console_scripts': [
            'ti.me=timecli.cli:main',
        ],
    },
)
