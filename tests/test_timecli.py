from unittest import TestCase
import subprocess

from dateutil.relativedelta import relativedelta

from timelib import (
    Manager,
    create_session,
    TaskPriority,
)
from timelib.exceptions import (
    InvalidActionError,
)
from timecli.parsing import (
    create_parser,
    parse_priority,
    parse_bool,
    parse_relativedelta,
)
from timecli.handling import root_handle


class TestTimeCli(TestCase):
    def setUp(self):
        self.manager = Manager(
            session=create_session('sqlite://'),
            default_user='default_user',
        )
        self.parser = create_parser()

    def cli(self, args):
        args = self.parser.parse_args(args)
        root_handle(self.manager, args)

    def test_shell(self):
        self.assertNotEqual(
            subprocess.call(
                ['ti.me'],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ),
            0,
        )
        self.assertNotEqual(
            subprocess.call(
                ['ti.me', 'something'],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ),
            0,
        )
        self.assertEqual(
            subprocess.call(
                ['ti.me', 'task', 'add', 'test task'],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ),
            0,
        )
        self.assertEqual(
            subprocess.call(
                ['ti.me', 'plan', 'add', '2 weeks'],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ),
            0,
        )

    def test_invalid_arguments(self):
        with self.assertRaises(SystemExit):
            self.cli(['something'])
        with self.assertRaises(SystemExit):
            self.cli(['task', 'something'])
        with self.assertRaises(SystemExit):
            self.cli(['planned-task', 'something'])
        with self.assertRaises(SystemExit):
            self.cli(['plan', 'something'])

    def test_create_task(self):
        self.cli(['task', 'add', 'test'])
        self.assertEqual(len(self.manager.get_tasks(text='test')), 1)

    def test_edit_task(self):
        self.cli(['task', 'add', 'test'])
        task = self.manager.get_tasks(text='test')[0]
        self.cli(['task', 'edit', str(task.id), '--text', 'edited'])
        self.assertEqual(len(self.manager.get_tasks(text='edited')), 1)
        self.cli(['task', 'complete', str(task.id)])
        self.assertEqual(len(self.manager.get_tasks(completed=True)), 1)
        self.cli(['task', 'edit', str(task.id), '--completed', 'no'])
        self.cli(['task', 'archive', str(task.id)])
        self.assertEqual(len(self.manager.get_tasks(archived=True)), 1)
        self.cli(['task', 'add', 'block'])
        self.cli(['task', 'add', 'target'])
        block = self.manager.get_tasks(text='block')[0]
        target = self.manager.get_tasks(text='target')[0]
        self.cli(['task', 'block', str(task.id), str(block.id)])
        self.cli(['task', 'target', str(task.id), str(target.id)])
        with self.assertRaises(InvalidActionError):
            self.cli(['task', 'complete', str(task.id)])
        self.cli(['task', 'complete', str(block.id)])
        self.cli(['task', 'complete', str(task.id)])
        self.assertEqual(len(self.manager.get_tasks(completed=True)), 3)
        self.cli(['task', 'share', str(task.id), 'stranger'])
        self.assertEqual(len(self.manager.get_tasks(user='stranger')), 1)

    def test_delete_task(self):
        self.cli(['task', 'add', 'test2'])
        self.assertEqual(len(self.manager.get_tasks()), 1)
        task = self.manager.get_tasks(text='test2')[0]
        self.cli(['task', 'delete', str(task.id)])
        self.assertEqual(len(self.manager.get_tasks()), 0)

    def test_create_planned_task(self):
        self.cli(['plan', 'add', '1 weeks'])
        plan = self.manager.get_plans()[0]
        self.cli(['planned-task', 'add', str(plan.id), 'test'])
        self.assertEqual(len(self.manager.get_planned_tasks()), 1)

    def test_edit_planned_task(self):
        self.cli(['plan', 'add', '1 weeks'])
        plan = self.manager.get_plans()[0]
        self.cli(['planned-task', 'add', str(plan.id), 'test'])
        task = self.manager.get_planned_tasks(plan_id=plan.id)[0]
        self.cli(['planned-task', 'edit', str(task.id), '--text', 'edited'])
        self.assertEqual(len(self.manager.get_planned_tasks(text='edited')), 1)
        self.cli(['planned-task', 'edit', str(task.id), '-m', 'stranger'])
        self.assertEqual(
            len(self.manager.get_planned_tasks(members=['stranger'])),
            1,
        )

    def test_delete_planned_task(self):
        self.cli(['plan', 'add', '1 weeks'])
        plan = self.manager.get_plans()[0]
        self.cli(['planned-task', 'add', str(plan.id), 'test'])
        task = self.manager.get_planned_tasks(plan_id=plan.id)[0]
        self.cli(['planned-task', 'delete', str(task.id)])
        self.assertEqual(len(self.manager.get_planned_tasks()), 0)

    def test_cleate_plan(self):
        self.cli(['plan', 'add', '1 weeks'])
        self.assertEqual(len(self.manager.get_plans()), 1)

    def test_edit_plan(self):
        self.cli(['plan', 'add', '1 weeks'])
        plan = self.manager.get_plans()[0]
        self.cli(['plan', 'edit', str(plan.id), '--count', '10'])
        self.assertEqual(plan.count, 10)

    def test_delete_plan(self):
        self.cli(['plan', 'add', '1 weeks'])
        plan = self.manager.get_plans()[0]
        self.cli(['plan', 'delete', str(plan.id)])
        self.assertEqual(len(self.manager.get_tasks()), 0)

    def test_parse(self):
        self.assertEqual(parse_priority('normal'), TaskPriority.NORMAL)
        with self.assertRaises(KeyError):
            parse_priority('something')
        self.assertTrue(parse_bool('yes'))
        self.assertTrue(parse_bool('true'))
        self.assertTrue(parse_bool('1'))
        self.assertFalse(parse_bool('no'))
        self.assertFalse(parse_bool('false'))
        self.assertFalse(parse_bool('0'))
        with self.assertRaises(ValueError):
            parse_bool('something')
        self.assertEqual(
            parse_relativedelta('2 weeks'),
            relativedelta(weeks=2),
        )
        self.assertEqual(
            parse_relativedelta('2 months, 4 hours, 4 days'),
            relativedelta(months=2, hours=4, days=4),
        )
        with self.assertRaises(ValueError):
            parse_relativedelta('2 weekday, 4 weeks')
        self.assertEqual(
            parse_relativedelta('2 seconds, 4 years'),
            relativedelta(seconds=2, years=4),
        )
        with self.assertRaises(ValueError):
            parse_relativedelta('something')
