from tempfile import mkdtemp
from os import path
from shutil import rmtree
from datetime import datetime
from unittest import TestCase

from dateutil.relativedelta import relativedelta

from timelib import (
    Manager,
    create_session,
    TaskPriority,
)
from timelib.exceptions import (
    NotFoundError,
    InvalidActionError,
    StorageError,
    DifferentPlanError,
    ActionAlreadyPerformedWarning,
)


class TestTimeLib(TestCase):
    def setUp(self):
        self.manager = Manager(
            session=create_session('sqlite://'),
            default_user='default_user',
        )

    def test_storage_fail(self):
        with self.assertRaises(StorageError):
            Manager(
                session=create_session('sqlite:////'),
                default_user='default_user',
            )

        directory = mkdtemp()
        manager = Manager(
            session=create_session(
                f'sqlite:///{path.join(directory, "storage.sqlite")}',
            ),
            default_user='default_user',
        )
        rmtree(directory)
        with self.assertRaises(StorageError):
            manager.reset_storage()

    def test_multiple_managers(self):
        first_manager = self.manager
        second_manager = Manager(
            session=self.manager.session,
            default_user='stranger',
        )

        task = first_manager.create_task(text='hello', members=['stranger'])
        first_manager.save()
        self.assertEqual(len(second_manager.get_tasks()), 1)
        self.assertEqual(task, second_manager.get_task(id=task.id))
        first_manager.complete_task(id=task.id)
        self.manager.save()
        self.assertTrue(second_manager.get_task(id=task.id).completed)

    def test_reset(self):
        self.manager.create_task()
        self.assertEqual(len(self.manager.get_tasks()), 1)
        self.manager.save()
        self.manager.reset_storage()
        self.assertEqual(len(self.manager.get_tasks()), 0)

    def test_as_stranger(self):
        with self.manager.save_after():
            self.manager.create_task(user='stranger')
        self.assertEqual(len(self.manager.get_tasks(user='stranger')), 1)

    def test_cancel(self):
        with self.assertRaises(Exception):
            with self.manager.save_after():
                self.manager.create_task()
                self.manager.get_tasks(from_time='yesterday')
        self.assertEqual(len(self.manager.get_tasks()), 0)

    def test_not_found(self):
        with self.assertRaises(NotFoundError):
            with self.manager.save_after():
                self.manager.get_planned_task(id=-1)
        with self.assertRaises(NotFoundError):
            with self.manager.save_after():
                self.manager.get_task(id=-1)
        with self.assertRaises(NotFoundError):
            with self.manager.save_after():
                self.manager.get_plan(id=-1)

    def test_create_task(self):
        task = self.manager.create_task()
        tasks = self.manager.get_tasks()
        self.assertEqual(len(tasks), 1)
        self.assertListEqual([task], tasks)
        self.manager.save()
        self.assertEqual(task, self.manager.get_task(id=task.id))

    def test_share_task(self):
        task = self.manager.create_task()
        self.manager.save()
        self.assertListEqual(task.members, [self.manager.default_user])
        with self.assertWarns(ActionAlreadyPerformedWarning):
            self.manager.share_task(
                id=task.id,
                members=['stranger', 'default_user'],
            )
        self.assertEqual(len(task.members), 2)
        self.assertEqual(
            set([self.manager.default_user, 'stranger']),
            set(task.members),
        )

    def test_complete_task(self):
        task = self.manager.create_task()
        self.manager.save()
        self.assertFalse(task.completed)
        self.manager.complete_task(id=task.id)
        with self.assertWarns(ActionAlreadyPerformedWarning):
            self.manager.complete_task(id=task.id)
        self.assertTrue(task.completed)

    def test_delete_task(self):
        related = self.manager.create_task()
        self.manager.save()
        task = self.manager.create_task(
            blocking_task_ids=[related.id],
        )

        with self.assertRaises(InvalidActionError):
            self.manager.delete_task(id=related.id)

        task.blocking_tasks.remove(related)
        self.manager.delete_task(id=related.id)
        self.assertEqual(len(self.manager.get_tasks()), 1)

    def test_complete_related_tasks(self):
        blocking = self.manager.create_task()
        target = self.manager.create_task()
        self.manager.save()
        main = self.manager.create_task(
            blocking_task_ids=[blocking.id],
            target_task_ids=[target.id],
        )
        self.manager.save()
        with self.assertRaises(InvalidActionError):
            self.manager.complete_task(id=main.id)
        self.manager.complete_task(id=blocking.id)
        self.manager.complete_task(id=main.id)
        self.assertTrue(target.completed)

    def test_create_planned_task(self):
        with self.manager.save_after():
            plan1 = self.manager.create_plan(delta=relativedelta(weeks=1))
            plan2 = self.manager.create_plan(delta=relativedelta(weeks=1))
        with self.manager.save_after():
            with self.assertWarns(ActionAlreadyPerformedWarning):
                task = self.manager.create_planned_task(
                    plan_id=plan1.id,
                    members=['default_user'],
                )
        with self.assertRaises(DifferentPlanError):
            self.manager.create_planned_task(
                plan_id=plan2.id,
                target_task_ids=[task.id],
            )
        with self.assertRaises(DifferentPlanError):
            self.manager.create_planned_task(
                plan_id=plan2.id,
                blocking_task_ids=[task.id],
            )
        self.assertEqual(task, self.manager.get_planned_task(id=task.id))

    def test_delete_planned_task(self):
        with self.manager.save_after():
            plan = self.manager.create_plan(delta=relativedelta(weeks=1))
        with self.manager.save_after():
            blocking = self.manager.create_planned_task(plan_id=plan.id)
        with self.manager.save_after():
            main = self.manager.create_planned_task(
                plan_id=plan.id,
                blocking_task_ids=[blocking.id],
            )
        with self.assertRaises(InvalidActionError):
            self.manager.delete_planned_task(id=blocking.id)

        self.manager.delete_planned_task(id=main.id)
        self.manager.delete_planned_task(id=blocking.id)
        self.assertEqual(len(self.manager.get_planned_tasks()), 0)

    def test_create_plan(self):
        plan = self.manager.create_plan(delta=relativedelta(days=1))
        plans = self.manager.get_plans()
        self.assertEqual(len(plans), 1)
        self.assertListEqual([plan], plans)

    def test_delete_plan(self):
        with self.manager.save_after():
            plan = self.manager.create_plan(delta=relativedelta(days=1))
        self.manager.create_planned_task(plan_id=plan.id)
        self.manager.delete_plan(id=plan.id)
        self.assertEqual(len(self.manager.get_plans()), 0)

    def test_updates(self):
        with self.manager.save_after():
            plan = self.manager.create_plan(
                delta=relativedelta(weeks=1),
                from_time=datetime.now() - relativedelta(weeks=3, days=1),
                to_time=datetime.now(),
            )
        with self.manager.save_after():
            self.manager.create_planned_task(plan_id=plan.id)
            self.manager.create_planned_task(plan_id=plan.id)

        updates = [*self.manager.get_updates()]
        self.assertEqual(len([*updates]), 4)
        self.assertEqual(
            len(self.manager.create_tasks_from_update(updates[0])),
            2,
        )

    def test_archive(self):
        task = self.manager.create_task()
        self.manager.create_task()
        self.manager.create_task()
        self.assertEqual(len(self.manager.get_tasks()), 3)
        self.assertEqual(len(self.manager.get_tasks(archived=False)), 3)
        self.assertEqual(len(self.manager.get_tasks(archived=True)), 0)
        task.archived = True
        self.assertEqual(len(self.manager.get_tasks()), 3)
        self.assertEqual(len(self.manager.get_tasks(archived=False)), 2)
        self.assertEqual(len(self.manager.get_tasks(archived=True)), 1)

    def test_priority(self):
        first = self.manager.create_task(priority=TaskPriority.CRITICAL)
        second = self.manager.create_task(priority=TaskPriority.IMPORTANT)
        third = self.manager.create_task()
        fourth = self.manager.create_task(priority=TaskPriority.LOW)
        self.assertListEqual(
            [first, second, third, fourth],
            self.manager.get_tasks(),
        )

    def test_labels(self):
        self.manager.create_task(labels=['tas.labs'])
        self.manager.create_task(labels=['tas.labs', 'tas.exam'])
        self.manager.create_task(labels=['tas.exam'])
        self.manager.create_task(labels=['tas'])
        self.assertEqual(len(self.manager.get_tasks(labels=['tas'])), 4)
        self.assertEqual(len(self.manager.get_tasks(labels=['tas.labs'])), 2)
        self.assertEqual(
            len(self.manager.get_tasks(labels=['tas.labs', 'tas.exam'])),
            1,
        )
