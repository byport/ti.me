import re

from dateutil.relativedelta import relativedelta
from django.forms import (
    TextInput,
    CharField,
    ValidationError,
)

from core.utils import serialize_relativedelta


class ListWidget(TextInput):
    def format_value(self, value):
        if isinstance(value, str):
            return value
        return ' '.join(value or [])


class ListField(CharField):
    widget = ListWidget

    def to_python(self, value):
        return [str(element) for element in (value or '').split()]


class RelativedeltaWidget(TextInput):
    def format_value(self, value):
        if isinstance(value, str):
            return value
        if value is None:
            return ''

        return serialize_relativedelta(value)


class RelativedeltaField(CharField):
    widget = RelativedeltaWidget

    def to_python(self, value):
        if value in self.empty_values:
            return None

        if re.match(
            (
                r'([+-]?\d+) (years|months|weeks|days|hours|minutes|seconds)'
                r'(,\s*([+-]?\d+)'
                r' (years|months|weeks|days|hours|minutes|seconds))*'
            ),
            value,
        ):
            try:
                return relativedelta(**{
                    key: int(value)
                    for value, key
                    in re.findall(
                        (
                            r'([+-]?\d+)'
                            r' (years|months|weeks|days|hours|minutes|seconds)'
                        ),
                        value,
                    )
                })
            except Exception:
                pass

        raise ValidationError('Invalid delta format', code='invalid')
