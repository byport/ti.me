from django.contrib.auth import views as auth_views
from django.conf.urls import url

from core import views


app_name = 'core'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(
        r'^login/',
        auth_views.LoginView.as_view(
            redirect_authenticated_user=True,
            template_name='core/login.html',
        ),
        name='login',
    ),
    url(r'^logout/', auth_views.logout_then_login, name='logout'),
    url(r'^signup/', views.signup, name='signup'),
]
