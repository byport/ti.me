from datetime import datetime

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.conf import settings
from dateutil.relativedelta import relativedelta

from timelib import (
    create_session,
    Manager,
)
from core.utils import date_only


def index(request):
    if request.user.is_authenticated():
        manager = Manager(
            create_session(
                f'sqlite:///{settings.DATABASES["default"]["NAME"]}',
            ),
            request.user.username,
        )

        context = {
            'today_tasks': manager.get_tasks(
                from_time=date_only(datetime.today()),
                to_time=date_only(datetime.today() + relativedelta(days=1)),
                archived=False,
            ),
            'week_tasks': manager.get_tasks(
                from_time=date_only(datetime.today()),
                to_time=date_only(datetime.today() + relativedelta(days=7)),
                archived=False,
            ),
            'plans': manager.get_plans(),
        }
    else:
        context = {}

    return render(
        request,
        'core/index.html',
        context,
    )


def signup(request):
    if request.user.is_authenticated():
        return redirect('core:index')

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('core:index')
    else:
        form = UserCreationForm()

    return render(request, 'core/signup.html', {'form': form})
