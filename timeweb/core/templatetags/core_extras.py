from django import template

from core.utils import serialize_relativedelta


register = template.Library()


@register.filter
def all_completed(tasks):
    return all(task.completed for task in tasks)


@register.filter
def relativedelta(value):
    return serialize_relativedelta(value)
