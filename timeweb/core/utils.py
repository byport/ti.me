from functools import wraps
from datetime import (
    datetime,
    time,
)

from django.conf import settings

from timelib import (
    create_session,
    Manager,
)


def serialize_relativedelta(value):
    string = ' '.join(
        f'{value} {key}'
        for key, value
        in {
            'years': value.years,
            'months': value.months,
            'weeks': value.weeks,
            'days': value.days,
            'hours': value.hours,
            'minutes': value.minutes,
            'seconds': value.seconds,
        }.items()
        if value
    )

    if not string:
        string = '0 hours'

    return string


def date_only(dt):
    return datetime.combine(dt, time())


def managed(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        manager = Manager(
            create_session(
                f'sqlite:///{settings.DATABASES["default"]["NAME"]}',
            ),
            request.user.username,
        )
        return func(request, manager=manager, *args, **kwargs)

    return wrapper
