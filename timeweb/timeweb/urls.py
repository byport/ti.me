from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^', include('core.urls')),
    url(r'^tasks/', include('tasks.urls')),
    url(r'^planned-tasks/', include('planned_tasks.urls')),
    url(r'^plans/', include('plans.urls')),
    url(r'^admin/', admin.site.urls),
]
