from django.conf.urls import url

from tasks import views


app_name = 'tasks'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add/$', views.create, name='create'),
    url(r'^search/$', views.search, name='search'),
    url(r'^(?P<task_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<task_id>[0-9]+)/edit/$', views.edit, name='edit'),
    url(r'^(?P<task_id>[0-9]+)/delete/$', views.delete, name='delete'),
    url(r'^(?P<task_id>[0-9]+)/share/$', views.share, name='share'),
    url(r'^(?P<task_id>[0-9]+)/complete/$', views.complete, name='complete'),
    url(
        r'^(?P<task_id>[0-9]+)/uncomplete/$',
        views.uncomplete,
        name='uncomplete',
    ),
    url(r'^(?P<task_id>[0-9]+)/archive/$', views.archive, name='archive'),
    url(
        r'^(?P<task_id>[0-9]+)/unarchive/$',
        views.unarchive,
        name='unarchive',
    ),
    url(
        r'^(?P<task_id>[0-9]+)/block/(?P<blocking_task_id>[0-9]+)/$',
        views.block,
        name='block',
    ),
    url(
        r'^(?P<task_id>[0-9]+)/archive/(?P<target_task_id>[0-9]+)/$',
        views.target,
        name='target',
    ),
]
