class SearchController {
    constructor() {
        self.abortController = new AbortController();
    }

    search(query) {
        self.abortController.abort();
        self.abortController = new AbortController();
        return fetch(
            `/tasks/search/?query=${query}`,
            {
                method: 'GET',
                signal: abortController.signal,
                credentials: 'include',
            },
        )
            .catch(console.warn)
            .then(res => res.json());
    }
}
