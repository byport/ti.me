from datetime import (
    datetime,
    time,
)

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse
from django.http import Http404, JsonResponse
from django.contrib.auth.models import User


from timelib.exceptions import (
    InvalidActionError,
    NotFoundError,
)

from tasks.forms import (
    CreateForm,
    EditForm,
    ShareForm,
    SearchForm,
)
from core.utils import managed


@login_required
@managed
def index(request, manager):
    users = User.objects.all()

    if request.method == 'POST':
        form = SearchForm(users, request.POST)
        if form.is_valid():
            tasks = manager.get_tasks(
                text=form.cleaned_data['text'],
                priority=form.cleaned_data['priority'] or None,
                completed=form.cleaned_data['completed'],
                archived=form.cleaned_data['archived'],
                members=form.cleaned_data['members'],
                labels=form.cleaned_data['labels'],
            )

            return render(
                request,
                'tasks/index.html',
                {'tasks': tasks, 'form': form},
            )
    else:
        form = SearchForm(users)
        tasks = manager.get_tasks(archived=False)

    return render(
        request,
        'tasks/index.html',
        {
            'tasks': tasks,
            'form': form,
        },
    )


@login_required
@managed
def detail(request, task_id, manager):
    users = User.objects.all()
    try:
        task = manager.get_task(id=int(task_id))
    except NotFoundError as e:
        raise Http404(e)

    return render(request, 'tasks/detail.html', {'task': task, 'users': users})


@login_required
@managed
def create(request, manager):
    users = User.objects.all()
    tasks = manager.get_tasks()

    if request.method == 'POST':
        form = CreateForm(users, tasks, request.POST)
        if form.is_valid():
            with manager.save_after():
                task = manager.create_task(
                    text=form.cleaned_data['text'],
                    from_time=form.cleaned_data['from_time'],
                    to_time=form.cleaned_data['to_time'],
                    priority=form.cleaned_data['priority'],
                    labels=form.cleaned_data['labels'],
                    members=form.cleaned_data['members'],
                    blocking_task_ids=list(
                        int(blocking_task)
                        for blocking_task
                        in form.cleaned_data['blocking_tasks']
                    ),
                    target_task_ids=list(
                        int(target_task)
                        for target_task
                        in form.cleaned_data['target_tasks']
                    ),
                )
            return redirect('tasks:detail', task.id)
    else:
        form = CreateForm(users, tasks)
    return render(
        request,
        'tasks/create.html',
        {
            'form': form,
        },
    )


@login_required
@managed
def edit(request, task_id, manager):
    users = User.objects.all()
    tasks = manager.get_tasks()
    task = manager.get_task(id=int(task_id))

    if request.method == 'POST':
        form = EditForm(users, tasks, request.POST)
        if form.is_valid():
            with manager.save_after():
                task.text = form.cleaned_data['text']
                task.from_time = form.cleaned_data.get(
                    'from_time',
                    task.from_time,
                )
                task.to_time = form.cleaned_data.get(
                    'to_time',
                    task.to_time,
                )
                task.priority = form.cleaned_data['priority']
                task.completed = form.cleaned_data['completed']
                task.archived = form.cleaned_data['archived']
                task.blocking_tasks = [
                    manager.get_task(id=int(blocking_task_id))
                    for blocking_task_id
                    in form.cleaned_data['blocking_tasks']
                ]
                task.target_tasks = [
                    manager.get_task(id=int(target_task_id))
                    for target_task_id
                    in form.cleaned_data['target_tasks']
                ]
                task.labels = form.cleaned_data['labels']
                task.members = list(
                    set(
                        form.cleaned_data['members']
                        + [request.user.username],
                    ),
                )
            return redirect(
                request.POST.get(
                    'next',
                    reverse('tasks:detail', args=[task_id]),
                ),
            )
    else:
        form = EditForm(
            users,
            tasks,
            initial={
                **vars(task),
                'priority': task.priority,
                'blocking_tasks': [
                    str(blocking_task.id)
                    for blocking_task
                    in task.blocking_tasks
                ],
                'target_tasks': [
                    str(target_task.id)
                    for target_task
                    in task.target_tasks
                ],
            },
        )

    return render(
        request,
        'tasks/edit.html',
        {
            'form': form,
            'task': task,
            'next': request.GET.get(
                'next',
                reverse('tasks:detail', args=[task_id]),
            ),
        },
    )


@login_required
@managed
def delete(request, task_id, manager):
    with manager.save_after():
        manager.delete_task(id=int(task_id))

    return redirect(
        request.GET.get('next', request.POST.get('next', 'tasks:index')),
    )


@login_required
@managed
def share(request, task_id, manager):
    task = manager.get_task(id=int(task_id))
    users = User.objects.all()

    if request.method == 'POST':
        form = ShareForm(users, request.POST)
        if form.data.get('member'):
            with manager.save_after():
                manager.share_task(
                    id=task.id,
                    members=[form.data['member']],
                )
            return redirect(
                request.POST.get(
                    'next',
                    reverse('tasks:detail', args=[task_id]),
                ),
            )

    return render(
        request,
        'tasks/share.html',
        {
            'task': task,
            'next': request.GET.get(
                'next',
                reverse('tasks:detail', args=[task_id]),
            ),
            'users': users,
        },
    )


@login_required
@managed
def complete(request, task_id, manager):
    try:
        with manager.save_after():
            task = manager.get_task(id=int(task_id))
            manager.complete_task(id=task.id)
    except InvalidActionError as e:
        return redirect(
            request.GET.get(
                "next",
                request.POST.get(
                    "next",
                    reverse("tasks:detail", args=[task_id]),
                ),
            )
            + f'?error={e}',
        )

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('tasks:detail', args=[task_id])),
        ),
    )


@login_required
@managed
def uncomplete(request, task_id, manager):
    with manager.save_after():
        task = manager.get_task(id=int(task_id))
        task.completed = False

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('tasks:detail', args=[task_id])),
        ),
    )


@login_required
@managed
def archive(request, task_id, manager):
    with manager.save_after():
        task = manager.get_task(id=int(task_id))
        task.archived = True

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('tasks:detail', args=[task_id])),
        ),
    )


@login_required
@managed
def unarchive(request, task_id, manager):
    with manager.save_after():
        task = manager.get_task(id=int(task_id))
        task.archived = False

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('tasks:detail', args=[task_id])),
        ),
    )


@login_required
@managed
def block(request, task_id, blocking_task_id, manager):
    with manager.save_after():
        task = manager.get_task(id=int(task_id))
        blocking = manager.get_task(id=int(blocking_task_id))
        task.blocking_tasks = list(set(task.blocking_tasks + [blocking]))

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('tasks:detail', args=[task_id])),
        ),
    )


@login_required
@managed
def target(request, task_id, target_task_id, manager):
    with manager.save_after():
        task = manager.get_task(id=int(task_id))
        target = manager.get_task(id=int(target_task_id))
        task.target_tasks = list(set(task.target_tasks + [target]))

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('tasks:detail', args=[task_id])),
        ),
    )


@login_required
@managed
def search(request, manager):
    def predicate(term, task):
        if term in task.text:
            return True
        elif term.startswith('@'):
            return term[1:] in task.members
        elif term.startswith('#'):
            return any(term[1:] == label or label.startswith(f'{term[1:]}.') for label in task.labels)
        elif all(letter == '!' for letter in term):
            return 5 - len(term) == task.priority
        elif term.lower() == 'completed':
            return task.completed
        elif term.lower() in ['uncompleted', 'incompleted']:
            return not task.completed
        elif term.lower() == 'archived':
            return task.archived
        elif term.lower() == 'unarchived':
            return not task.archived
        elif term.isdigit():
            return int(term) == task.id

        return False

    tasks = [
        {
            'id': task.id,
            'completed': task.completed,
            'text': task.text,
            'from_time': task.from_time,
            'to_time': task.to_time,
            'archived': task.archived,
            'priority': task.priority,
            'created_at': task.created_at,
            'updated_at': task.updated_at,
            'labels': task.labels,
            'members': task.members,
            'blocking_tasks': [blocking_task.id for blocking_task in task.blocking_tasks],
            'target_tasks': [target_task.id for target_task in task.target_tasks],
        }
        for task
        in manager.get_tasks()
        if all(
            predicate(term, task)
            for term
            in request.GET.get('query', '').split(' ')
        )
    ]

    return JsonResponse(tasks, safe=False)
