from django.forms import (
    Form,
    CharField,
    ChoiceField,
    BooleanField,
    NullBooleanField,
    Textarea,
    RadioSelect,
    DateTimeField,
    DateTimeInput,
    MultipleChoiceField,
)

from timelib import TaskPriority

from core.forms import ListField


class CreateForm(Form):
    text = CharField(label='Description', widget=Textarea)
    from_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='From',
    )
    to_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='To',
        required=False,
    )
    priority = ChoiceField(
        label='Priority',
        widget=RadioSelect(attrs={'class': 'form-check-input'}),
        choices=[
            (priority, priority.name.capitalize())
            for priority
            in TaskPriority
        ],
        initial=TaskPriority.NORMAL,
    )
    labels = ListField(label='Labels', required=False)
    members = MultipleChoiceField(label='Members', required=False)
    blocking_tasks = MultipleChoiceField(label='Blocking tasks', required=False)
    target_tasks = MultipleChoiceField(label='Target tasks', required=False)

    def __init__(self, users, tasks, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['members'].choices = [(user.username, user.username) for user in users]
        self.fields['blocking_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in tasks]
        self.fields['target_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in tasks]


class EditForm(Form):
    text = CharField(
        label='Description',
        widget=Textarea,
        required=False,
    )
    from_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='From',
        required=False,
    )
    to_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='To',
        required=False,
    )
    priority = ChoiceField(
        label='Priority',
        widget=RadioSelect(attrs={'class': 'form-check-input'}),
        choices=[
            (priority, priority.name.capitalize())
            for priority
            in TaskPriority
        ],
        required=False,
    )
    completed = BooleanField(label='Completed', required=False)
    archived = BooleanField(label='Archived', required=False)
    labels = ListField(label='Labels', required=False)
    members = MultipleChoiceField(label='Members', required=False)
    blocking_tasks = MultipleChoiceField(label='Blocking tasks', required=False)
    target_tasks = MultipleChoiceField(label='Target tasks', required=False)

    def __init__(self, users, tasks, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['members'].choices = [(user.username, user.username) for user in users]
        self.fields['blocking_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in tasks]
        self.fields['target_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in tasks]


class ShareForm(Form):
    member = ChoiceField(label='Share with')

    def __init__(self, users, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['members'].choices = [(user.username, user.username) for user in users]


class SearchForm(Form):
    query = CharField(label='Query')
