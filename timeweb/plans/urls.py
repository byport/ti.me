from django.conf.urls import url

from plans import views


app_name = 'plans'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add/$', views.create, name='create'),
    url(r'^check/$', views.check, name='check'),
    url(r'^(?P<plan_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<plan_id>[0-9]+)/edit/$', views.edit, name='edit'),
    url(r'^(?P<plan_id>[0-9]+)/delete/$', views.delete, name='delete'),
]
