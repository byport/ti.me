from datetime import (
    datetime,
    time,
)

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse

from plans.forms import (
    CreateForm,
    EditForm,
)
from core.utils import managed


@login_required
@managed
def index(request, manager):
    plans = manager.get_plans()

    return render(request, 'plans/index.html', {'plans': plans})


@login_required
@managed
def detail(request, plan_id, manager):
    plan = manager.get_plan(id=int(plan_id))

    return render(request, 'plans/detail.html', {'plan': plan})


@login_required
@managed
def create(request, manager):
    if request.method == 'POST':
        form = CreateForm(request.POST)
        if form.is_valid():
            with manager.save_after():
                plan = manager.create_plan(
                    delta=form.cleaned_data['delta'],
                    from_time=form.cleaned_data['from_time'],
                    to_time=form.cleaned_data['to_time'],
                    count=form.cleaned_data['count'],
                )
            return redirect('plans:detail', plan.id)
    else:
        form = CreateForm()
    return render(request, 'plans/create.html', {'form': form})


@login_required
@managed
def edit(request, plan_id, manager):
    plan = manager.get_plan(id=int(plan_id))

    if request.method == 'POST':
        form = EditForm(request.POST)
        if form.is_valid():
            with manager.save_after():
                plan.delta = form.cleaned_data['delta']
                plan.from_time = form.cleaned_data.get(
                    'from_time',
                    plan.from_time,
                )
                plan.to_time = form.cleaned_data.get(
                    'to_time',
                    None,
                )
                plan.count = form.cleaned_data['count']
            return redirect(
                request.POST.get(
                    'next',
                    reverse('plans:detail', args=[plan_id]),
                ),
            )
    else:
        form = EditForm(initial=vars(plan))

    return render(
        request,
        'plans/edit.html',
        {
            'form': form,
            'plan': plan,
            'next': request.GET.get(
                'next',
                reverse('plans:detail', args=[plan_id]),
            ),
        },
    )


@login_required
@managed
def delete(request, plan_id, manager):
    with manager.save_after():
        manager.delete_plan(id=int(plan_id))

    return redirect(
        request.GET.get('next', request.POST.get('next', 'plans:index')),
    )


@login_required
@managed
def check(request, manager):
    with manager.save_after():
        for update in manager.get_updates():
            with manager.save_after():
                manager.create_tasks_from_update(update)

    return redirect(
        request.GET.get('next', request.POST.get('next', 'plans:index')),
    )
