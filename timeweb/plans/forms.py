from django.forms import (
    Form,
    IntegerField,
    DateTimeField,
    DateTimeInput,
)

from core.forms import RelativedeltaField


class CreateForm(Form):
    delta = RelativedeltaField(label='Delta')
    from_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='From',
    )
    to_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='To',
        required=False,
    )
    count = IntegerField(
        label='Count',
        required=False,
    )


class EditForm(Form):
    delta = RelativedeltaField(label='Delta')
    from_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='From',
        required=False,
    )
    to_time = DateTimeField(
        widget=DateTimeInput(attrs={'type': 'datetime-local'}),
        input_formats=['%Y-%m-%dT%H:%M'],
        label='To',
        required=False,
    )
    count = IntegerField(label='Count', required=False)
