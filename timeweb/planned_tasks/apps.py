from django.apps import AppConfig


class PlannedTasksConfig(AppConfig):
    name = 'planned_tasks'
