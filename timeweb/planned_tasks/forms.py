from django.forms import (
    Form,
    ChoiceField,
    CharField,
    BooleanField,
    Textarea,
    RadioSelect,
    IntegerField,
    MultipleChoiceField,
)
from timelib import TaskPriority

from core.forms import ListField, RelativedeltaField


class CreateForm(Form):
    plan_id = IntegerField(label='Plan')
    text = CharField(label='Description', widget=Textarea)
    from_time = RelativedeltaField(label='From')
    to_time = RelativedeltaField(label='To', required=False)
    priority = ChoiceField(
        label='Priority',
        widget=RadioSelect(attrs={'class': 'form-check-input'}),
        choices=[
            (priority, priority.name.capitalize())
            for priority
            in TaskPriority
        ],
        initial=TaskPriority.NORMAL,
    )
    labels = ListField(label='Labels', required=False)
    members = MultipleChoiceField(label='Members', required=False)
    blocking_tasks = MultipleChoiceField(label='Blocking tasks', required=False)
    target_tasks = MultipleChoiceField(label='Target tasks', required=False)

    def __init__(self, users, planned_tasks, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['members'].choices = [(user.username, user.username) for user in users]
        self.fields['blocking_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in planned_tasks]
        self.fields['target_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in planned_tasks]


class EditForm(Form):
    plan_id = IntegerField(label='Plan')
    text = CharField(label='Description', widget=Textarea)
    from_time = RelativedeltaField(label='From')
    to_time = RelativedeltaField(label='To', required=False)
    priority = ChoiceField(
        label='Priority',
        widget=RadioSelect(attrs={'class': 'form-check-input'}),
        choices=[
            (priority, priority.name.capitalize())
            for priority
            in TaskPriority
        ],
        required=False,
    )
    completed = BooleanField(label='Completed', required=False)
    archived = BooleanField(label='Archived', required=False)
    labels = ListField(label='Labels', required=False)
    members = MultipleChoiceField(label='Members', required=False)
    blocking_tasks = MultipleChoiceField(label='Blocking tasks', required=False)
    target_tasks = MultipleChoiceField(label='Target tasks', required=False)

    def __init__(self, users, planned_tasks, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['members'].choices = [(user.username, user.username) for user in users]
        self.fields['blocking_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in planned_tasks]
        self.fields['target_tasks'].choices = [(task.id, f'#{task.id} {task.text}') for task in planned_tasks]
