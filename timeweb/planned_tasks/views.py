from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User

from planned_tasks.forms import (
    CreateForm,
    EditForm,
)
from core.utils import managed


@login_required
@managed
def detail(request, planned_task_id, manager):
    planned_task = manager.get_planned_task(id=int(planned_task_id))

    return render(
        request,
        'planned_tasks/detail.html',
        {'planned_task': planned_task},
    )


@login_required
@managed
def create(request, manager):
    users = User.objects.all()

    if request.method == 'POST':
        planned_tasks = manager.get_planned_tasks(plan_id=int(request.POST['plan_id']))
        form = CreateForm(users, planned_tasks, request.POST)
        if form.is_valid():
            with manager.save_after():
                planned_task = manager.create_planned_task(
                    plan_id=form.cleaned_data['plan_id'],
                    text=form.cleaned_data['text'],
                    from_time=form.cleaned_data['from_time'],
                    to_time=form.cleaned_data['to_time'],
                    priority=form.cleaned_data['priority'],
                    labels=form.cleaned_data['labels'],
                    members=form.cleaned_data['members'],
                    blocking_task_ids=list(
                        int(blocking_task)
                        for blocking_task
                        in form.cleaned_data['blocking_tasks']
                    ),
                    target_task_ids=list(
                        int(target_task)
                        for target_task
                        in form.cleaned_data['target_tasks']
                    ),
                )
            return redirect('planned_tasks:detail', planned_task.id)
    else:
        planned_tasks = manager.get_planned_tasks(plan_id=int(request.GET['plan_id']))
        form = CreateForm(users, planned_tasks, initial={'plan_id': request.GET.get('plan_id')})

    return render(request, 'planned_tasks/create.html', {'form': form})


@login_required
@managed
def edit(request, planned_task_id, manager):
    planned_task = manager.get_planned_task(id=int(planned_task_id))
    users = User.objects.all()
    planned_tasks = manager.get_planned_tasks(plan_id=planned_task.plan_id)

    if request.method == 'POST':
        form = EditForm(users, planned_tasks, request.POST)
        if form.is_valid():
            with manager.save_after():
                planned_task.plan_id = form.cleaned_data['plan_id']
                planned_task.text = form.cleaned_data['text']
                planned_task.from_time = form.cleaned_data['from_time']
                planned_task.to_time = form.cleaned_data['to_time']
                planned_task.priority = form.cleaned_data['priority']
                planned_task.completed = form.cleaned_data['completed']
                planned_task.archived = form.cleaned_data['archived']
                planned_task.blocking_tasks = [
                    manager.get_planned_task(id=int(blocking_task_id))
                    for blocking_task_id
                    in form.cleaned_data['blocking_tasks']
                ]
                planned_task.target_tasks = [
                    manager.get_planned_task(id=int(target_task_id))
                    for target_task_id
                    in form.cleaned_data['target_tasks']
                ]
                planned_task.labels = form.cleaned_data['labels']
                planned_task.members = list(
                    set(
                        form.cleaned_data['members']
                        + [request.user.username],
                    ),
                )
            return redirect(
                request.POST.get(
                    'next',
                    reverse('planned_tasks:detail', args=[planned_task_id]),
                ),
            )
    else:
        form = EditForm(
            users,
            planned_tasks,
            initial={
                **vars(planned_task),
                'priority': planned_task.priority,
                'blocking_tasks': [
                    str(blocking_task.id)
                    for blocking_task
                    in planned_task.blocking_tasks
                ],
                'target_tasks': [
                    str(target_task.id)
                    for target_task
                    in planned_task.target_tasks
                ],
            },
        )

    return render(
        request,
        'planned_tasks/edit.html',
        {
            'form': form,
            'planned_task': planned_task,
            'next': request.GET.get(
                'next',
                reverse('planned_tasks:detail', args=[planned_task_id]),
            ),
        },
    )


@login_required
@managed
def delete(request, planned_task_id, manager):
    with manager.save_after():
        planned_task = manager.get_planned_task(id=int(planned_task_id))
        plan = planned_task.plan
        manager.delete_planned_task(id=planned_task.id)

    return redirect(
        request.GET.get(
            'next',
            request.POST.get('next', reverse('plans:detail', args=[plan.id])),
        ),
    )
