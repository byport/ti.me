from django.conf.urls import url

from planned_tasks import views


app_name = 'planned_tasks'
urlpatterns = [
    url(r'^add/$', views.create, name='create'),
    url(r'^(?P<planned_task_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<planned_task_id>[0-9]+)/edit/$', views.edit, name='edit'),
    url(r'^(?P<planned_task_id>[0-9]+)/delete/$', views.delete, name='delete'),
]
