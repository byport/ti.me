# ti.me

Manage, organize, share and plan your tasks using ti.me task manager.

## Getting started

You can use ti.me in three different ways:

* **timelib** library for embedding ti.me capabilities in your own app;
* **timecli** tool for managin your tasks using command-line interface;
* **[timeweb](http://ti.byport.me)** app for managing your tasks everywhere.

### Prerequisites

You need at least Python 3.6.

### Installing

You can install ti.me using pip:

```bash
pip install ti.me
```

For development purposes you can clone this repository:

```bash
git clone https://git@bitbucket.org/byport/ti.me.git
```

And install it locally:

```bash
pip install -e ./ti.me
```

Check if timecli is working:
```bash
ti.me --help
```

## Built with

* **SQLAlchemy** -- SQL Toolkit and Object Relational Mapper;
* **dateutil** -- powerful extensions to the standard datetime module;
* **termcolor** -- ANSII Color formatting for output in terminal.

## Configuration

To configure timecli, use `config.json` file (by default located in `$HOME/.ti.me` directory). Also you can find defults and constants in `timecli/config.py` file.

## Usage

### timelib

```python
from datetime import datetime

from timelib import (
    Manager,
    create_session,
    TaskPriority,
)


manager = Manager(
    session=create_session('sqlite://'),
    default_user='stranger',
)

with manager.save_after():
    lab3task = manager.create_task(
        text='Pass tas lab #3',
        from_time=datetime(2018, 08, 24),
        labels=['labs.tas'],
        priority=TaskPriority.IMPORTANT,
    )
with manager.save_after():
    lab4task = manager.create_task(
        text='Pass tas lab #4',
        from_time=datetime(2018, 08, 24),
        labels=['labs.tas'],
        priority=TaskPriority.IMPORTANT,
        blocking_task_ids=[lab3task],
    )
with manager.save_after():
    examtask = manager.create_task(
        text='Pass tas exam',
        from_time=datetime(2018, 09, 08),
        labels=['edu.tas'],
        priority=TaskPriority.CRITICAL,
        blocking_task_ids=[lab3task, lab4task],
    )

for labtask in manager.get_tasks(labels=['labs']):
    print(task.text)
```

### timecli

```bash
ti.me task add "Pass tas lab #3" --from "2018-08-24" --labels labs.tas --priority important
ti.me task add "Pass tas lab #4" -f "2018-08-24" -l labs.tas -p important -bt 1
ti.me task add "Pass tas exam" -f "2018-09-08" -l edu.tas -p critical -bt 1 2
ti.me task list
```

## License

This project is licensed under the MIT License -- see the [LICENSE](https://bitbucket.org/byport/ti.me/src/master/LICENSE) file for details.
